export interface Clientes {
    id: number;
    clientName: string;
    clientEmail: string;
    phoneNumber: string;
    documentNumber: string;
}