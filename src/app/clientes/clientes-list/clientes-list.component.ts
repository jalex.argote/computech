import { Component, OnInit } from '@angular/core';
import { Clientes } from '../interfaces/clientes';
import { ClientesService } from '../clientes.service';

@Component({
  selector: 'app-clientes-list',
  templateUrl: './clientes-list.component.html',
  styleUrls: ['./clientes-list.component.styl']
})
export class ClientesListComponent implements OnInit {

  public clientesList: Clientes [];
  constructor(private clientesServer: ClientesService) { }


  ngOnInit() {
    this.clientesServer.query()
    .subscribe(res =>{
      this.clientesList = res;
      console.log('Response Data', res)
    },
    error => console.error('Error', error)
    )
  }

}
