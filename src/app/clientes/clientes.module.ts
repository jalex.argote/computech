import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientesRoutingModule } from './clientes-routing.module';
import { ClientesListComponent } from './clientes-list/clientes-list.component';
import { ClientesCreateComponent } from './clientes-create/clientes-create.component';
import { ClientesUpdateComponent } from './clientes-update/clientes-update.component';
import { ClientesViewComponent } from './clientes-view/clientes-view.component';



@NgModule({
  declarations: [ClientesListComponent, ClientesCreateComponent, ClientesUpdateComponent, ClientesViewComponent],
  imports: [
    CommonModule,
    ClientesRoutingModule
  ]
})
export class ClientesModule { }
