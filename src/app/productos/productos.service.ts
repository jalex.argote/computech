import { Injectable } from '@angular/core';
import { Productos } from './interfaces/productos'
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {

  constructor(private http: HttpClient) { }

  public query(): Observable<Productos[]>{
    return this.http.get<Productos[]>(`${environment.END_POINT}/api/bike`)
    .pipe(map(res => {
      return res;
    }));
  }
}
