import { Component, OnInit } from '@angular/core';
import { Productos } from '../interfaces/productos';
import { ProductosService } from '../productos.service';

@Component({
  selector: 'app-productos-list',
  templateUrl: './productos-list.component.html',
  styleUrls: ['./productos-list.component.styl']
})
export class ProductosListComponent implements OnInit {

  
  public productosList: Productos [];
  constructor(private productosServer: ProductosService) { }

  ngOnInit() {
    this.productosServer.query()
    .subscribe(res => {
      this.productosList = res;
      console.log('Respose Data',res)
    },
    error => console.error('Error', error)
    );
  }

}
