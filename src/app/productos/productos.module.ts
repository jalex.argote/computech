import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductosListComponent } from './productos-list/productos-list.component';
import { ProductosCreateComponent } from './productos-create/productos-create.component';
import { ProductosUpdateComponent } from './productos-update/productos-update.component';
import { ProductosViewComponent } from './productos-view/productos-view.component';
import { ProductosRoutingModule } from './productos-routing.module';



@NgModule({
  declarations: [ProductosListComponent, ProductosCreateComponent, ProductosUpdateComponent, ProductosViewComponent],
  imports: [
    CommonModule,
    ProductosRoutingModule
  ]
})
export class ProductosModule { }
