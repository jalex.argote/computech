import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductosListComponent } from './productos-list/productos-list.component';
import { ProductosCreateComponent } from './productos-create/productos-create.component';
import { ProductosUpdateComponent } from './productos-update/productos-update.component';
import { ProductosViewComponent } from './productos-view/productos-view.component';


const routes: Routes = [
    {
        path: 'productos-list',
        component: ProductosListComponent

    },
    {
        path: 'productos-create',
        component: ProductosCreateComponent

    },
    {
        path: 'productos-update',
        component: ProductosUpdateComponent

    },
    {
        path: 'productos-view',
        component: ProductosViewComponent

    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProductosRoutingModule { }
